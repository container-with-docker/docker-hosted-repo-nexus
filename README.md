# Docker-Hosted-Repo-Nexus



## Purpose
Creating a Docker repository on Nexus Repository Manager and pushing containarized application image to it

```
Workflow
- Creat Docker hosted repository on Nexus
- Create Docker repository role on Nexus
- Configure Nexus, DigitalOcean Droplet and docker, so one is able to push to Docker hosted repo on Nexus.
- Build and push Docker image to docker repo on Nexus
```
```
cd existing_repo
git remote add origin https://gitlab.com/container-with-docker/docker-hosted-repo-nexus.git
git branch -M main
git push -uf origin main
```

